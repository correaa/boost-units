#ifdef COMPILATION_INSTRUCTIONS
(echo "#include\""$0"\"" > $0x.cpp) && c++ -O3 -fconcepts -std=c++17 -Wall -fmax-errors=2 `#-Wfatal-errors` -I${HOME}/prj -D_TEST_ALF_BOOST_UNITS_CMATH $0x.cpp -o $0x.x && time $0x.x $@ && rm -f $0x.x $0x.cpp; exit
#endif
#ifndef ALF_BOOST_UNITS_CMATH_HPP
#define ALF_BOOST_UNITS_CMATH_HPP

#include<boost/units/cmath.hpp>
//#include "alf/boost/units/cmath/pow.hpp"

namespace boost{
namespace units{

template<class S, class Y, typename = decltype(quantity<si::dimensionless, Y>(quantity<S, Y>{})) >
inline quantity<S, Y> log(quantity<S, Y> const& q)
{
    using std::log;

//    typedef quantity<BOOST_UNITS_DIMENSIONLESS_UNIT(S), Y> quantity_type;

//    return quantity<S, Y>::from_value(log(q.value()));
	return log(q.value());
}

//	template<int N, int D, class Unit, typename Y> 
//inline auto pow(boost::units::quantity<Unit, Y> const& q)->decltype(boost::units::pow<static_rational<N, D> >(q)){
//	return boost::units::pow<static_rational<N, D> >(q);
//}

template<class Unit, typename Y>
decltype(auto) root3(quantity<Unit, Y> const& q){
	return pow<static_rational<1, 3>>(q);
}
template<class Unit, typename Y>
decltype(auto) cbrt(quantity<Unit, Y> const& q){
	return pow<static_rational<1, 3>>(q);
}

template<class Unit, typename Y>
decltype(auto) root23(quantity<Unit, Y> const& q){
	return pow<static_rational<2, 3>>(q);
}

template<class Unit, typename Y>
decltype(auto) pow2(quantity<Unit, Y> const& q){return boost::units::pow<2>(q);}

template<class U, class T>
decltype(auto) operator*(quantity<U, T> const& q, int d){return q*T(d);}
template<class U, class T>
decltype(auto) operator*(int f, quantity<U, T> const& q){return T(f)*q;}
template<class U, class T>
decltype(auto) operator/(quantity<U, T> const& q, int d){return q/T(d);}
template<class U, class T>
decltype(auto) operator/(int n, quantity<U, T> const& q){return T(n)/q;}

}}

#ifdef _TEST_ALF_BOOST_UNITS_CMATH

#include<boost/units/systems/si.hpp>

#include<cassert>

int main(){
	using boost::units::si::meter;
	assert( cbrt(9.*pow<3>(meter)) == cbrt(9.)*meter );
	assert( 4*(3.*meter) == 12.*meter );
}
#endif
#endif

