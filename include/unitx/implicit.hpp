#ifdef COMPILATION_INSTRUCTIONS
(echo "#include\""$0"\"" > $0x.cpp) && c++ -std=c++14 -Wfatal-errors -D_TEST_BOOST_UNITS_IMPLICIT $0x.cpp -o $0x.x && $0x.x $@; rm -f $0x.cpp $0x.x; exit
#endif
#ifndef BOOST_UNITS_IMPLICIT_HPP
#define BOOST_UNITS_IMPLICIT_HPP
// by including this header you will enable implicit conversion between boost::units::quantities, which is not the default behavior
#include<boost/units/quantity.hpp>

#if 0 // this is the old way of allowing implicit conversion by erasing the keyword explicit
// make quantity conversion implicit by default
#ifndef BOOST_UNITS_QUANTITY_HPP
#define explicit
#include<boost/units/quantity.hpp>
#undef explicit
#else
// #error "Please, `#include \"boost/units/implicit.hpp\"` before `#include<boost/units/quantity.hpp>`, otherwise the requested implicit conversion will not work"
#endif
#endif

namespace boost{
namespace units{

#ifndef BOOST_UNITS_NO_ALLOW_IMPLICIT_CONVERSIONS
#pragma message "implicit conversions are allowed"
template<
	class U1, class U2, 
	typename = decltype(boost::units::conversion_factor(U1{}, U2{}))
>
std::true_type is_conversion_factor_helper(U1 const&, U2 const&);
std::false_type is_conversion_factor_helper(...);

// this overwrites the definition of boost::units::is_implicitly_convertible to allow all possible conversions implicitly
// to understand how it works, look at the constructor of boost::units::quantity
// seems to have a problem in gcc when compiled in some contexts (???)
template<class... U1, class... U2>
struct is_implicitly_convertible<unit<U1...>, unit<U2...>> : 
decltype(is_conversion_factor_helper(unit<U1...>{}, unit<U2...>{})){};
#endif

//template<class U1, class U2, typename = typename std::enable_if<not std::is_same<U1,U2>::value>::type>
//auto operator+=(quantity<U1>& q1, quantity<U2> const& q2)->decltype(q1+=quantity<U1>(q2)){
//	return q1+=quantity<U1>(q2);
//}

struct implicit{

// forwarder for convertible quantity
template<class... QuantityParams>
struct quantity : boost::units::quantity<QuantityParams...>{
	template<class... Args> quantity(Args&&... args) 
	: boost::units::quantity<QuantityParams...>(std::forward<Args>(args)...){}
	operator boost::units::quantity<QuantityParams...>&(){return static_cast<boost::units::quantity<QuantityParams...>&>(*this);}
	operator boost::units::quantity<QuantityParams...> const&() const{return static_cast<boost::units::quantity<QuantityParams...> const&>(*this);}
};

};

}}

#ifdef _TEST_BOOST_UNITS_IMPLICIT
////////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include<boost/units/systems/si/io.hpp>
#include<boost/units/systems/cgs/io.hpp>
#include<complex>

using namespace boost::units;

double f(implicit:: quantity<cgs::length> const& x){
	std::cout << "what " << x << std::endl;
	return x.value();
}

double g(quantity<cgs::length> const& x){
	std::cout << "what " << x << std::endl;
	return x.value();
}

int main(){}

#endif
#endif

