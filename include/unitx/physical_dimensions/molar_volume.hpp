#ifndef BOOST_UNITS_MOLAR_VOLUME_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_MOLAR_VOLUME_DERIVED_DIMENSION_HPP

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/length.hpp>
#include <boost/units/physical_dimensions/mass.hpp>
#include <boost/units/physical_dimensions/time.hpp>
#include <boost/units/physical_dimensions/amount.hpp>

namespace boost {

namespace units {

/// derived dimension for molar volume : L^3 N^-1
typedef derived_dimension<length_base_dimension,3,
                          amount_base_dimension,-1>::type molar_volume_dimension;                    

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_MOLAR_VOLUME_DERIVED_DIMENSION_HPP
