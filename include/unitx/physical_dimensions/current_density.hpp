#ifndef BOOST_UNITS_CURRENT_DENSITY_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_CURRENT_DENSITY_DERIVED_DIMENSION_HPP

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/length.hpp>
#include <boost/units/physical_dimensions/current.hpp>

namespace boost {
namespace units {

/// derived dimension for current density : L^-3 I
typedef derived_dimension<length_base_dimension,-2,
                          current_base_dimension,1>::type
current_density_dimension;
                          
//typedef current_density_dimension ...

} // namespace units
} // namespace boost

#endif // BOOST_UNITS_CURRENT_DENSITY_DERIVED_DIMENSION_HPP

