#ifndef BOOST_UNITS_AU_REDUCED_PLANCK_CONSTANT_BASE_UNIT_HPP
#define BOOST_UNITS_AU_REDUCED_PLANCK_CONSTANT_BASE_UNIT_HPP

#include <string>

#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/action.hpp>

namespace boost {

namespace units {

namespace au {

struct reduced_planck_constant_base_unit : base_unit<
	reduced_planck_constant_base_unit, action_dimension, 
	-18
>{
	static std::string name()   { return (
		"planckbar" 
	//	"reduced Planck constant"
	);}
	static std::string symbol() { return ("hbar");}
};

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_ELEMENTARY_CHARGE_BASE_UNIT_HPP

