// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2013-2023 Alfredo Correa
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_SYSTEMS_NONSI_ELECTRON_VOLT_BASE_UNIT_HPP
#define BOOST_UNITS_SYSTEMS_NONSI_ELECTRON_VOLT_BASE_UNIT_HPP

#include "unitx/systems/au/io.hpp"

#include <string>


#include <boost/units/config.hpp>
#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/energy.hpp>
//#include <boost/units/base_units/si/joule.hpp>
#include<boost/units/systems/si/energy.hpp>

#include <boost/units/systems/si/codata/electromagnetic_constants.hpp> // codata::e

#include <boost/units/conversion.hpp>
#if 0
BOOST_UNITS_DEFINE_BASE_UNIT_WITH_CONVERSIONS(nonsi, angstrom, "angstrom", "A", 1.e-10, si::meter_base_unit, -401);    // exact conversion
q
#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TYPE(boost::units::nonsi::angstrom_base_unit)

#endif
#endif

namespace boost{
namespace units{
namespace nonsi {

struct electronvolt_base_unit :
    boost::units::base_unit<electronvolt_base_unit, energy_dimension, 993>
{
    static std::string name()       { return "electronvolt"; }
    static std::string symbol()     { return "eV"; }
};

typedef boost::units::make_system<electronvolt_base_unit>::type system_ev;

/// unit typedefs
typedef unit<energy_dimension,system_ev>    energy;
typedef unit<energy_dimension,system_ev>    electronvolt_unit;

static const energy electronvolt, eV;

[[deprecated("use electronvolt")]]
static const energy electron_volt;

} // nonsi
}}

// helper for conversions between nonsi energy and si energy
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(nonsi::electronvolt_base_unit,
                                    boost::units::si::energy,
                                    double, boost::units::si::constants::codata::e.value().value() /*1.602176634e-19*/ /*1./6.241509e18*/ );  // https://en.wikipedia.org/wiki/Electronvolt
BOOST_UNITS_DEFAULT_CONVERSION(
	nonsi::electronvolt_base_unit,
	boost::units::si::energy
);

namespace boost {

namespace units { 

inline std::string name_string(const reduce_unit<decltype(nonsi::eV/au::atom)>::type&) { return "electronvolt per atom"; }
inline std::string symbol_string(const reduce_unit<decltype(nonsi::eV/au::atom)>::type&) { return "eV/atom"; }

}}

#endif // BOOST_UNITS_SYSTEMS_NONSI_ELECTRON_VOLT_BASE_UNIT_HPP
