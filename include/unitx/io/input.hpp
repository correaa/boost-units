#ifdef compile_instructions
(echo '#include"'$0'"'>$0.cpp)&& c++ -std=c++11 -O3 -Wall -Wfatal-errors -D_TEST_BOOST_UNITS_IO_INPUT -I$HOME/prj $0.cpp -o $0x &&$0x&& rm $0.cpp $0x; exit
#endif
#ifndef BOOST_UNITS_IO_INPUT_HPP
#define BOOST_UNITS_IO_INPUT_HPP
// © Alfredo Correa 2019

#include<iostream>
#include<boost/units/io.hpp>
#include<boost/units/systems/si/dimensionless.hpp> // for input on division

namespace boost{
namespace units{

template<class U, class T>
std::istream& operator>>(std::istream& is, quantity<U, T>& q){
	typename quantity<U, T>::value_type val; is >> val;
	while(is.peek()==' ') is.get(); // ignore all spaces
	auto const expected = to_string(U{});
	std::string tag(expected.size(), 0); is.get(&tag[0], tag.size() + 1);
	if(tag==expected) q = quantity<U, T>::from_value(val);
	else{
	//	std::cerr<<"error expected "<< expected <<", got "<< tag <<std::endl;
		is.setstate(std::ios::failbit);
	}
	return is;
}

template<class Q, class U2>
struct div_expression{
	using value_type = typename Q::value_type;
	div_expression(div_expression const&) = delete;
	div_expression(div_expression&& o) : div_expression{o.q_}{}
	explicit div_expression(Q& q) : q_{q}{}
	Q& q_;
	friend std::istream& operator>>(std::istream& is, div_expression&& de){
		value_type d; is >> d;
		de.q_ = Q(d*U2{});
		return is;
	}
	div_expression const& operator=(value_type const& d) const{
		q_ = d*U2{}; 
		return *this;
	}
	operator value_type() const{return q_/U2{};}
};

#if 1
template<class Q>
struct div_expression<Q, typename Q::unit_type>{
	using value_type = typename Q::value_type;
	div_expression(div_expression const&) = delete;
	div_expression(div_expression&& o) : div_expression{o.q_}{}
	explicit div_expression(Q& q) : q_{q}{}
	Q& q_;
	operator value_type&()&&{return const_cast<value_type&>(q_.value());}
	div_expression&& operator=(value_type const& d)&&{
		std::move(*this).operator value_type&() = d; return std::move(*this);
	}
};
#endif

template<class U, class T, class U2, typename = decltype(quantity<si::dimensionless, T>(boost::units::operator/(std::declval<quantity<U, T>>(), U2{})))>
div_expression<quantity<U, T>, U2> div(quantity<U, T>& q, U2 const&){
	return div_expression<quantity<U, T>, U2>{q};
}

template<class U, class T, class U2, typename = decltype(quantity<si::dimensionless, T>(boost::units::operator/(std::declval<quantity<U, T>>(), U2{})))>
div_expression<quantity<U, T>, U2> operator/(quantity<U, T>& q, U2 const&){
	return div_expression<quantity<U, T>, U2> {q};
}

}}

#ifdef _TEST_BOOST_UNITS_IO_INPUT

//#include<boost/units/systems/si/io.hpp>
//#include<boost/units/systems/cgs/io.hpp>
#include"alf/boost/units/base_units/nonsi.hpp"

#include<boost/lexical_cast.hpp>

using namespace boost::units;

int main(){
	{
		quantity<si::length> l = 3.14*si::meter;
		std::stringstream ss; ss<< l <<"abc"<<std::endl;

		quantity<si::length> l2; ss >> l2; 
		assert(ss and l == l2);

		std::string check; ss >> check; assert(check=="abc");
	}
	{
		auto e = 5.24*si::joule;
		std::stringstream ss; ss<< e <<"abc"<<std::endl;
		// promise not do funny stuff between write and read, like including new units/io headers
		quantity<si::energy> e2; ss >> e2; 
		assert(ss and quantity<si::energy>{e} == e2);

		std::string check; ss >> check; assert(check=="abc");
	}
	{
		std::istringstream iss{"5"};
		quantity<si::length> l3; iss >> l3/si::meter;
		assert(l3 == 5.*si::meter); 
		l3/si::meter = 6.;
		assert(l3 == 6.*si::meter);
	}
	{
		std::istringstream iss{"5"};
		quantity<si::length> l; iss >> l/(si::micro*si::meter);
		assert( (l - 5.e-6*si::meter) < 1e-22*si::meter ); 
	}
	{
		std::istringstream iss{"4.34 s"};
		quantity<si::time> t; iss >> t;
		assert( t == 4.34*si::second );
	}
	{
		auto t = boost::lexical_cast<quantity<si::time>>("4.34 s");
		assert( t == 4.34*si::second );
	}
	{
		auto en = 1.*nonsi::eV;
		std::istringstream iss{"13. eV"};
		iss >> en;
		assert( en == 13.*nonsi::eV );
	}
}

#endif
#endif

