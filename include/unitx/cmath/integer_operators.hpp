#ifdef compile_instructions
ln -sf $0 $0.cpp && c++ -std=c++14 -Wfatal-errors -Wall -Wfatal-errors -D_TEST_BOOST_UNITS_INTEGER_OPERATOR $0.cpp -o $0.x && $0.x $@; exit
#endif

/// extends operators on quantities
#ifndef BOOST_UNITS_INTEGER_OPERATOR
#define BOOST_UNITS_INTEGER_OPERATOR
#include<boost/units/quantity.hpp>
#include<boost/units/systems/si.hpp>
#include<cassert>
namespace boost{
namespace units{
// allow division and multiplication by integer, indirectly this allows to compute mean of boost::inverval of boost::units::quantity
#define DECLARE_INT_POST_OPERATOR(SymboL) \
	template<class U, class T, \
		class Int, \
		typename = std::enable_if_t<std::is_integral<Int>{}> \
	> \
	auto operator SymboL (quantity<U, T> const& q, Int divisor) \
		->decltype(q SymboL T(divisor)){ \
		return q SymboL T(divisor); \
	}
	DECLARE_INT_POST_OPERATOR(/)
	DECLARE_INT_POST_OPERATOR(*)
#undef DECLARE_POST_OPERATOR

#define DECLARE_INT_PRE_OPERATOR(SymboL) \
	template<class U, class T, \
		class Int, \
		typename = std::enable_if_t<std::is_integral<Int>{}> \
	> \
	auto operator SymboL (Int divisor, quantity<U, T> const& q) \
		->decltype(T(divisor) SymboL q){ \
		return T(divisor) SymboL q; \
	}
	DECLARE_INT_PRE_OPERATOR(/)
	DECLARE_INT_PRE_OPERATOR(*)
#undef DECLARE_PRE_OPERATOR

}}

#ifdef _TEST_BOOST_UNITS_INTEGER_OPERATOR

#include<boost/units/systems/si/io.hpp>
#include<boost/units/cmath.hpp>

//#include "alf/boost/units/interval.hpp"
//#include<boost/numeric/interval/io.hpp>
//#include "alf/boost/numeric/interval.hpp"
#include<iostream>
using namespace boost::units;
using std::cout;
int main(){
	auto v = 3.*pow<3>(si::meter);
	cout 
		<< quantity<si::length>(1.*si::meter)/2. << '\n'
		<< quantity<si::length>(1.*si::meter)/2 << '\n'
		<< quantity<si::length>(1.*si::meter)*2 << '\n'
		<< quantity<si::dimensionless>(1.) - 1 << '\n'
		<< cbrt(v/v) - 1 << '\n'
//		<< boost::numeric::make_interval(1.*si::meter, 2.*si::meter) << '\n'
//		<< median(boost::numeric::make_interval(1.*si::meter, 2.*si::meter)) << '\n'
//		<< 2*(1.*si::meter) << '\n'
//		<< 2*(1.*si::meter) << '\n'
//		<< (1.*si::meter) << '\n'
	;
}
#endif
#endif

