#ifndef BOOST_UNITS_AU_TIME_HPP
#define BOOST_UNITS_AU_TIME_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/time.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<time_dimension,au::system>      time;

BOOST_UNITS_STATIC_CONSTANT(au_of_time,time); 
BOOST_UNITS_STATIC_CONSTANT(time_unit,time); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_TIME_HPP

