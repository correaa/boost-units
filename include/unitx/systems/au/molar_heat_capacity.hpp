#ifndef BOOST_UNITS_AU_MOLAR_HEAT_CAPACITY_HPP
#define BOOST_UNITS_AU_MOLAR_HEAT_CAPACITY_HPP

#include "../../systems/au/base.hpp"
#include<boost/units/physical_dimensions/amount.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<
//molar_heat_capacity_dimension,
	derived_dimension<length_base_dimension,2,
                          mass_base_dimension,1,
                          time_base_dimension,-2,
                          temperature_base_dimension,-1,
                          amount_base_dimension,-1>::type,
	au::system>      molar_heat_capacity;
    
} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_MOLAR_HEAT_CAPACITY_HPP

