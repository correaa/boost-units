#ifndef BOOST_UNITS_AU_PLANE_ANGLE_HPP
#define BOOST_UNITS_AU_PLANE_ANGLE_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/plane_angle.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<plane_angle_dimension,au::system>      plane_angle;
    
BOOST_UNITS_STATIC_CONSTANT(au_of_plane_angle,plane_angle); 
BOOST_UNITS_STATIC_CONSTANT(radian,plane_angle); 
BOOST_UNITS_STATIC_CONSTANT(radians,plane_angle); 

} // namespace au

} // namespace units

} // namespace boost

#endif

