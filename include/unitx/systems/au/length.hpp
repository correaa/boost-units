#ifndef BOOST_UNITS_AU_LENGTH_HPP
#define BOOST_UNITS_AU_LENGTH_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/length.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<length_dimension,au::system>      length;
    
BOOST_UNITS_STATIC_CONSTANT(bohr,length); 
BOOST_UNITS_STATIC_CONSTANT(bohrs,length); 
BOOST_UNITS_STATIC_CONSTANT(bohr_radius,length);
BOOST_UNITS_STATIC_CONSTANT(bohr_radii,length);

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_LENGHT_HPP

