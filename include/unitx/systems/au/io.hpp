#ifndef BOOST_UNITS_AU_IO_HPP
#define BOOST_UNITS_AU_IO_HPP

#include "unitx/systems/au.hpp"

#include <boost/units/io.hpp>
#include <boost/units/reduce_unit.hpp>

namespace boost {

namespace units { 

inline std::string name_string  (const reduce_unit<au::elastivity>::type&){return "coulombforceconstant";}
inline std::string symbol_string(const reduce_unit<au::elastivity>::type&){return "k_e";}

inline std::string name_string  (const reduce_unit<au::energy>::type&){return "hartree";}
inline std::string symbol_string(const reduce_unit<au::energy>::type&){return "E_h";}

inline std::string name_string  (const reduce_unit<au::length>::type&){return "bohr";}
inline std::string symbol_string(const reduce_unit<au::length>::type&){return "a_0";}

inline std::string name_string  (const reduce_unit<au::time>::type&){return "planckbar per hartree";}
inline std::string symbol_string(const reduce_unit<au::time>::type&){return "hbar / E_h";}

inline std::string name_string  (const reduce_unit<au::velocity>::type&){return "bohr hartree per planckbar";}
inline std::string symbol_string(const reduce_unit<au::velocity>::type&){return "a_0 E_h / hbar";}

inline std::string name_string  (const reduce_unit<au::force>::type&){return "hartree per bohr";}
inline std::string symbol_string(const reduce_unit<au::force>::type&){return "E_h / a_0";}

inline std::string name_string  (const reduce_unit<au::temperature>::type&){return "hartree per boltzmannconstant";}
inline std::string symbol_string(const reduce_unit<au::temperature>::type&){return "E_h / k_B";}

inline std::string name_string  (const reduce_unit<au::pressure>::type&){return "atomic unit of pressure";}
inline std::string symbol_string(const reduce_unit<au::pressure>::type&){return "E_h / a_0^3";}

inline std::string name_string  (const reduce_unit<au::volume>::type&){return "atomic unit of volume";}
inline std::string symbol_string(const reduce_unit<au::volume>::type&){return "a_0^3";}

inline std::string name_string  (const reduce_unit<au::molar_heat_capacity>::type&){return "atomic unit of molar heat capacity";}
inline std::string symbol_string(const reduce_unit<au::molar_heat_capacity>::type&){return "k_B / \\mathrm{atom}";}

inline std::string name_string  (const reduce_unit<au::molar_energy>::type&){return "atomic unit of molar heat capacity";}
inline std::string symbol_string(const reduce_unit<au::molar_energy>::type&){return symbol_string(au::energy()) + symbol_string(au::amount()) + "^-1";}

inline std::string name_string  (const reduce_unit<au::molarity>::type&){return "mole per cubic bohr";}// "atomic unit of molarity";}
inline std::string symbol_string(const reduce_unit<au::molarity>::type&){return symbol_string(au::amount()) + "/" + symbol_string(au::volume());}
	
inline std::string name_string  (const reduce_unit<au::wavenumber>::type&){return "reciprocal bohr";}
inline std::string symbol_string(const reduce_unit<au::wavenumber>::type&){return "a_0^-1";}

inline std::string name_string  (const reduce_unit<au::area>::type&){return "square bohr";}
inline std::string symbol_string(const reduce_unit<au::area>::type&){return "a_0^2";}

inline std::string name_string  (const reduce_unit<au::inverse_area>::type&){return "per square bohr";}
inline std::string symbol_string(const reduce_unit<au::inverse_area>::type&){return "a_0^{-2}";}

inline std::string name_string  (const reduce_unit<au::inverse_volume>::type&){return "per cubic bohr";}
inline std::string symbol_string(const reduce_unit<au::inverse_volume>::type&){return "a_0^{-3}";}

inline std::string name_string  (const reduce_unit<au::frequency>::type&) 
//{ return "atomic unit of frequency"; }
{ return "hartree per planckbar"; }
inline std::string symbol_string(const reduce_unit<au::frequency>::type&) { return symbol_string(au::energy()) +  "/" + symbol_string(au::action()); }

inline std::string name_string  (const reduce_unit<au::mass_flow>::type&){return "electronmass hartree per planckbar";}
inline std::string symbol_string(const reduce_unit<au::mass_flow>::type&){return "m_e E_h / hbar";}


} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_IO_HPP

