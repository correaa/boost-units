#ifndef BOOST_UNITS_AU_FREQUENCY_HPP
#define BOOST_UNITS_AU_FREQUENCY_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/frequency.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<frequency_dimension,au::system>      frequency;

BOOST_UNITS_STATIC_CONSTANT(au_of_frequency,frequency); 
BOOST_UNITS_STATIC_CONSTANT(frequency_unit,frequency); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_FREQUENCY_HPP

