#ifndef BOOST_UNITS_AU_HEAT_CAPACITY_HPP
#define BOOST_UNITS_AU_HEAT_CAPACITY_HPP

#include "../../systems/au/base.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<heat_capacity_dimension,au::system>      heat_capacity;
    
BOOST_UNITS_STATIC_CONSTANT(boltzmann_constant,heat_capacity); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_HEAT_CAPACITY_HPP

