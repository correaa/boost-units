#ifdef compile_instructions
ln -sf $0 $0.cpp && clang++ -std=c++11 -Wfatal-errors -D_TEST_BOOST_UNITS_ATOMIC_ABBREVIATIONS $0.cpp -o $0.x && $0.x $@; rm -f $0.cpp $0.x; exit
#endif

#ifndef BOOST_UNITS_ATOMIC_ABBREVIATIONS_HPP
#define BOOST_UNITS_ATOMIC_ABBREVIATIONS_HPP

#include "../au.hpp"
#include<boost/units/systems/si.hpp>
#include<boost/units/systems/si/prefixes.hpp>
#include "alf/boost/units/systems/si/abbreviations.hpp"

namespace boost{namespace units{

namespace au{

namespace abbreviations{

#if __clang__
#define BOOST_UNITS_DECLARE_POWERS(a) \
	static const auto          a##2 = pow<2>(a); \
	static const auto          a##3 = pow<3>(a); \
	static const auto          a##² = pow<2>(a); \
	static const auto          a##³ = pow<3>(a); \
	static const auto          a##⁻¹ = pow<-1>(a); \
	static const auto          a##⁻² = pow<-2>(a); \
	static const auto          a##⁻³ = pow<-3>(a);
#endif

	//static const length B; // already a synonim for a_0
	static const length          a_0, a0, B, a_B, aB;
#if __clang__
BOOST_UNITS_DECLARE_POWERS(a_0)
BOOST_UNITS_DECLARE_POWERS(a0)
BOOST_UNITS_DECLARE_POWERS(B)
BOOST_UNITS_DECLARE_POWERS(a_B)
BOOST_UNITS_DECLARE_POWERS(aB)
#endif

	static const energy          E_h, Eh;
	static const heat_capacity   k_B, kB;
	static const action          hbar;
	static const auto hbar2 = pow<2>(hbar);
	static const auto hbar3 = pow<3>(hbar);
#if __clang__
	static const auto hbar² = pow<2>(hbar);
	static const auto hbar³ = pow<3>(hbar);
	static const auto hbar⁻¹ = pow<-1>(hbar);
	static const auto hbar⁻² = pow<-2>(hbar);
	static const auto hbar⁻³ = pow<-3>(hbar);

	static const action         ℏ;
BOOST_UNITS_DECLARE_POWERS(ℏ)
#endif
	static const electric_charge e;

#if __clang__
BOOST_UNITS_DECLARE_POWERS(e);
#endif

	static const plane_angle     rad, c;

}


}

namespace nonsi{

namespace abbreviations{
	//static const auto 
	//B_over_fs = atomic::bohr/(si::femto*si::second);
}

}

}}

#ifdef _TEST_BOOST_UNITS_ATOMIC_ABBREVIATIONS
#include <boost/units/systems/cgs.hpp>
#include <boost/units/io.hpp>
#include <iostream>
#include "alf/boost/units/systems/au/io.hpp"

using namespace boost::units;
using namespace boost::units::au::abbreviations;
using namespace std;

int main(){
	clog << 4.*a_0*E_h/hbar*k_B << endl;
	clog << 4.*a_0*E_h/hbar*k_B*e⁻² << endl;

	clog << 4.*hbar2 << endl;
	clog << 4.*ℏ⁻¹ << endl;
	clog << aB³ << endl;
}
#endif
#endif

