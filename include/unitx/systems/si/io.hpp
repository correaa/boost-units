#ifndef ALF_BOOST_UNITS_SI_IO_HPP
#define ALF_BOOST_UNITS_SI_IO_HPP

#include <../include/boost/units/io.hpp>
#include <boost/units/reduce_unit.hpp>

#include "alf/boost/units/systems/si.hpp"
#include <boost/units/systems/si/io.hpp>

namespace boost {
namespace units { 

// tries to replace silly Gy (gray) alternative interpretation of energy per kilogram as absored dose
//inline std::string name_string(const reduce_unit<si::specific_energy>::type&) { return "joule per kilogram"; }
//inline std::string symbol_string(const reduce_unit<si::specific_energy>::type&) { return "J kg^-1"; }

/*
inline std::string name_string(const reduce_unit<si::specific_heat_capacity>::type&) { return "joule per kilogram per kelvin"; }
inline std::string symbol_string(const reduce_unit<si::specific_heat_capacity>::type&) { return "J kg^-1 K^-1"; }

inline std::string name_string(const reduce_unit<si::heat_capacity>::type&) { return "joule per kelvin"; }
inline std::string symbol_string(const reduce_unit<si::heat_capacity>::type&) { return "J K^-1"; }

inline std::string name_string(const reduce_unit<si::conductivity>::type&) { return "siemens per meter"; }
inline std::string symbol_string(const reduce_unit<si::conductivity>::type&) { return "S m^-1"; }
*/

//inline std::string name_string(const reduce_unit<si::resistivity>::type&) { return "ohm meter"; }
//inline std::string symbol_string(const reduce_unit<si::resistivity>::type&) { return "Ohm m"; }

//inline std::string name_string(const reduce_unit<si::angular_velocity>::type&) { return "radian per second"; }
//inline std::string symbol_string(const reduce_unit<si::angular_velocity>::type&) { return "rad s^-1"; }

inline std::string name_string(const reduce_unit<si::dimensionless>::type&) { return "dimensionless"; }
inline std::string symbol_string(const reduce_unit<si::dimensionless>::type&) { return "U"; }

} // namespace units

} // namespace boost

#endif // ALF_BOOST_UNITS_SI_IO_HPP
