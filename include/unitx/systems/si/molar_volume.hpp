#ifndef BOOST_UNITS_SI_MOLAR_VOLUME_HPP
#define BOOST_UNITS_SI_MOLAR_VOLUME_HPP

#include <boost/units/systems/si/base.hpp>

#include "alf/boost/units/physical_dimensions/molar_volume.hpp"

namespace boost {

namespace units { 

namespace si {

typedef unit<molar_volume_dimension,si::system>  molar_volume;

BOOST_UNITS_STATIC_CONSTANT(cubic_meter_per_mole , molar_volume);
BOOST_UNITS_STATIC_CONSTANT(cubic_meter_per_mol  , molar_volume);
BOOST_UNITS_STATIC_CONSTANT(cubic_metre_per_mole, molar_volume);
BOOST_UNITS_STATIC_CONSTANT(cubic_metre_per_mol , molar_volume);

inline std::string name_string(const reduce_unit<si::molar_volume>::type&)   { return "cubic meter per mole";}
//inline std::string symbol_string(const reduce_unit<si::molar_volume>::type&) { return "m^3 mol^-1";}

} // namespace si

} // namespace units

} // namespace boost

#endif

