#ifndef BOOST_UNITS_SI_SPECIFIC_VOLUME_HPP
#define BOOST_UNITS_SI_SPECIFIC_VOLUME_HPP

#include <boost/units/systems/si/base.hpp>

#include <boost/units/physical_dimensions/molar_energy.hpp>

namespace boost {

namespace units { 

namespace si {

typedef unit<molar_energy_dimension,si::system>  molar_energy;

BOOST_UNITS_STATIC_CONSTANT(joule_per_mole , molar_energy);
BOOST_UNITS_STATIC_CONSTANT(joule_per_mol  , molar_energy);
BOOST_UNITS_STATIC_CONSTANT(joules_per_mole, molar_energy);
BOOST_UNITS_STATIC_CONSTANT(joules_per_mol , molar_energy);

inline std::string name_string(const reduce_unit<si::molar_energy>::type&)   { return /*"Gibbs"*/ "joule per mole"; }
inline std::string symbol_string(const reduce_unit<si::molar_energy>::type&) { return /*"G"    */ "J mol^-1"         ; }

} // namespace si

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_SI_SPECIFIC_VOLUME_HPP

