#include <boost/core/lightweight_test.hpp>

#include <unitx/systems/au.hpp>
#include <unitx/systems/au/io.hpp>

#include <boost/units/io.hpp>
#include <boost/units/systems/si.hpp>

using namespace boost::units;

void test_au_base_units() { // base units using default symbol_format (no
                            // format specified) and no auto prefixing.
                            // #define FORMATTERS
  {
    std::ostringstream ss;
    ss << 1.1 * au::electron_mass << '\n'
       << 2.2 * au::elementary_charge << '\n'
       << 3.3 * au::reduced_planck_constant << '\n'
       << 4.4 * au::coulomb_force_constant << '\n'
       << 5.5 * au::bohr_radius << '\n'
       << 6.6 * au::hartree_energy << '\n'
       << 7.7 * au::boltzmann_constant << '\n'
       << 8.8 * au::molecule << '\n';

    BOOST_TEST_EQ(ss.str(), "1.1 m_e\n"
                            "2.2 e\n"
                            "3.3 hbar\n"
                            "4.4 k_e\n"
                            "5.5 a_0\n"
                            "6.6 E_h\n"
                            "7.7 k_B\n"
                            "8.8 atom\n");
  }

  // BOOST_UNITS_TEST_OUTPUT(meter_base_unit::unit_type(), "m");
  // BOOST_UNITS_TEST_OUTPUT(velocity(), "m s^-1");
  // BOOST_UNITS_TEST_OUTPUT(scaled_length(), "km");
  // BOOST_UNITS_TEST_OUTPUT(scaled_velocity1(), "k(m s^-1)");
  // BOOST_UNITS_TEST_OUTPUT(millisecond_base_unit::unit_type(), "ms");
  // BOOST_UNITS_TEST_OUTPUT(scaled_time(), "ms");
  // BOOST_UNITS_TEST_OUTPUT(scaled_velocity2(), "m ms^-1");
  // BOOST_UNITS_TEST_OUTPUT(area(), "m^2");
  // BOOST_UNITS_TEST_OUTPUT(scaled_area(), "k(m^2)");
  // BOOST_UNITS_TEST_OUTPUT(double_scaled_length(), "Kikm");
  // BOOST_UNITS_TEST_OUTPUT(double_scaled_length2(), "kscm");
  // BOOST_UNITS_TEST_OUTPUT(custom1(), "c1");
  // BOOST_UNITS_TEST_OUTPUT(custom2(), "c2");
  // BOOST_UNITS_TEST_OUTPUT(scaled_custom1(), "kc1");
  // BOOST_UNITS_TEST_OUTPUT(scaled_custom2(), "kc2");
  // BOOST_UNITS_TEST_OUTPUT(boost::units::absolute<meter_base_unit::unit_type>(),
  // "absolute m");
  // #undef FORMATTERS
}

void test_au_important_derived_units() {
  {
    std::ostringstream ss;
    ss << 1.1 * au::hartree_energy << '\n'
       << 2.2 * au::bohr_radius << '\n'
       << 3.3 * au::coulomb_force_constant << '\n';

    BOOST_TEST_EQ(ss.str(), "1.1 E_h\n"
                            "2.2 a_0\n"
                            "3.3 k_e\n");
  }
}

void test_au_unnamed_derived_units() {
  {
    std::ostringstream ss;
    ss << 1.1 * au::reduced_planck_constant / au::hartree_energy << '\n'
       << 2.2 * au::bohr_radius * au::hartree_energy /
              au::reduced_planck_constant
       << '\n'
       << 3.3 * au::hartree_energy / au::bohr_radius << '\n'
       << 4.4 * au::hartree_energy / au::boltzmann_constant << '\n'
       << 5.5 * au::hartree_energy / au::bohr_radius / au::bohr_radius /
              au::bohr_radius
       << '\n';

    BOOST_TEST_EQ(ss.str(), "1.1 hbar / E_h\n"
                            "2.2 a_0 E_h / hbar\n"
                            "3.3 E_h / a_0\n"
                            "4.4 E_h / k_B\n"
                            "5.5 E_h / a_0^3\n");
  }
  {
    std::ostringstream ss;
    ss << 1.1 * au::hartree << '\n'
       << 2.2 * au::boltzmann_constant / au::electron_mass << '\n'
       << 3.3 * au::boltzmann_constant << '\n'
       << 4.4 * au::molecule << '\n'
       << 5.5 * au::molecule / au::hartree
       << '\n'
       //  //		<< 4.*au::boltzmann_constant/au::molecule << '\n'
       << 6.6 * au::hartree / au::molecule << '\n'
       << 7.7 * au::hartree / au::species << '\n'
       << 8.8 * au::radian << '\n';
    BOOST_TEST_EQ(ss.str(), "1.1 E_h\n"
                            "2.2 m_e^-1 k_B\n"
                            "3.3 k_B\n"
                            "4.4 atom\n"
                            "5.5 hbar^2 atom e^-4 m_e^-1 k_e^-2\n"
                            "6.6 hbar^-2 atom^-1 e^4 m_e k_e^2\n"
                            "7.7 hbar^-2 atom^-1 e^4 m_e k_e^2\n"
                            "8.8 rad\n");
  }
}

int main() {
  test_au_base_units();
  test_au_important_derived_units();
  test_au_unnamed_derived_units();
  // test_input_unit_raw();
  // test_input_unit_name();
  // test_input_quantity_symbol();
  // test_input_quantity_raw();
  // test_input_quantity_name();
  // test_input_autoprefixed_quantity_name();
  // test_input_autoprefixed_quantity_symbol();
  // test_input_auto_binary_prefixed_quantity_symbol();
  // test_input_auto_binary_prefixed_quantity_name();
  // test_input_quantity_name_duplicate();
  // test_input_quantity_symbol_duplicate();
  // test_input_auto_binary_prefixed_quantity_name_duplicate();
  // test_input_auto_binary_prefixed_quantity_symbol_duplicate();
  // test_input_typename_format();
  return boost::report_errors();
}